// pages/jump/index.js
Page({
  data: {
    options: {},
    number: ""
  },
  onLoad(options) {
    if (options) {
      options.path = decodeURIComponent(options.path)
      options.fail = function (err) {
        console.log(err)
      }
      this.setData({
        options
      })
    }
  },
  jump() {
    this.data.options.path = decodeURIComponent(this.options.path) + "&lpn=" + this.data.number
    wx.navigateToMiniProgram(this.data.options)
  }
})