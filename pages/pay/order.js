// pages/pay/index.js
Page({
  getOpenId(code, cookie, money,id,params) {
    wx.request({
      url: 'https://shop.zhengjiashangye.cn/app/index.php?i=2&c=entry&m=ewei_shopv2&do=mobile&r=member.recharge.getOpenid',
      data: {
        code
      },
      header: {
        cookie
      },
      method: "GET",
      success: (res) => {
        const openid = res.data.userInfo.openid
        this.sendPay(params)
        // this.submit2(cookie, money, openid,id,params)
      }
    })

  },
  submit(cookie, money, openid,id) {
    wx.request({
      url: 'https://shop.zhengjiashangye.cn/app/index.php?i=2&c=entry&m=ewei_shopv2&do=mobile&r=order.pay.check',
      method: "POST",
      data: '\r\n--XXX' +
        '\r\nContent-Disposition: form-data; name="id"' + //第一个参数key1:val1,固定写法以此类推
        '\r\n' +
        '\r\n' + id +
        '\r\n--XXX--',
      header: {
        cookie,
        'content-type': 'multipart/form-data; boundary=XXX', //XXX是分隔符的意思
      },
      success: (res) => {
        this.submit2(cookie, money, openid,id,res.data.result.url)
      }
    })
  },
  submit2(cookie, money, openid,id,params) {
    wx.request({
      url:"https://shop.zhengjiashangye.cn/app/index.php?i=2&c=entry&m=ewei_shopv2&do=mobile&r=order.pay&id=" + id,
      method: "GET",
      data: '\r\n--XXX' +
      '\r\nContent-Disposition: form-data; name="type"' + //第一个参数key1:val1,固定写法以此类推
      '\r\n' +
      '\r\n' + 'wechat_mp' +
      '\r\n--XXX' +
      '\r\nContent-Disposition: form-data; name="money"' +
      '\r\n' +
      '\r\n' + +money +
      '\r\n--XXX' +
      '\r\nContent-Disposition: form-data; name="couponid"' +
      '\r\n' +
      '\r\n' + openid +
      '\r\n--XXX--' +
      '\r\nContent-Disposition: form-data; name="id"' +
      '\r\n' +
      '\r\n' + id +
      '\r\n--XXX--',
      header: {
        cookie,
        'content-type': 'multipart/form-data; boundary=XXX', //XXX是分隔符的意思
      },
      success: (res) => {
        this.sendPay(res.data.result.wechat)
      }
    })
  },
  sendPay(params) {
    console.log(params)
    params = JSON.parse(params)
    params.package = decodeURIComponent(params.package)
    console.log("------------------")
    console.log(params)
    wx.requestPayment({
      nonceStr: params.nonceStr,
      package: params.package,
      paySign: params.paySign,
      timeStamp: params.timeStamp,
      signType: params.signType,
      success: function (res) {
        console.log('res: ', res);
        wx.showToast({
          title: '支付成功',
        })
        setTimeout(() => {
          wx.navigateTo({
            url: '/pages/index/index',
          })
        }, 2000);
      },
      fail: function (err) {
        console.log('err: ', err);
        wx.showToast({
          title: '支付失败',
        })
        setTimeout(() => {
          wx.navigateTo({
            url: '/pages/index/index',
          })
        }, 2000);
      }
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options)
    options.package = decodeURIComponent(options.package)
    options.cookie = decodeURIComponent(options.cookie)
    this.sendPay(options.params)
    // 登录 获取code
    // wx.login({
    //   timeout: 2000,
    // }).then((res) => {
    //   this.getOpenId(res.code, options.cookie, options.money, options.id, options.params)
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    console.log(this)
  },

})