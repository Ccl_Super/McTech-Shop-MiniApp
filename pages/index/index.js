// index.js
Page({
  data: {
    webviewurl: ""
  },
  getMsg(res) {
  },
  onShareAppMessage(options) {
    const webViewUrl = options.webViewUrl
    const path = `/pages/index/index?url=${encodeURIComponent(webViewUrl)}`
    return {
      title: "正嘉高新都荟",
      path,
      imageUrl: "",
    }
  },
  onLoad(options) {
    if (options.url) {
      this.setData({ webviewurl:decodeURIComponent(options.url)})
    }else{
      this.setData({ webviewurl:'https://shop.zhengjiashangye.cn/app/index.php?i=2&c=entry&m=ewei_shopv2&do=mobile'})
    }
  }
})